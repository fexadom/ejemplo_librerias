LDIR = libs
INCLUDE = include

prog: $(LDIR)/libvector.a main2.c
	gcc -c main2.c -I$(INCLUDE)
	gcc -static -o prog main2.o $(LDIR)/libvector.a

$(LDIR)/libvector.a: $(LDIR)/addvec.c $(LDIR)/multvec.c
	gcc -c $(LDIR)/addvec.c $(LDIR)/multvec.c
	ar rcs $(LDIR)/libvector.a addvec.o multvec.o

.PHONY: clean
clean:
	rm -rf *.o prog
	rm -rf $(LDIR)/*.a
